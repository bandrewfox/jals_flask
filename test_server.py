import time

from jals_obj import JustAnotherLiteServer


if __name__ == '__main__':
    jals = JustAnotherLiteServer("D:\\temp")
    for i in range(1, 6):
        if not jals.spawn():
            print "Cannot spawn %i" % i

    num_running = 1
    num_big_loops = 1000
    while num_running > 0:
        time.sleep(5)
        num_running = 0
        for cur_port in jals.ports:
            cur_port_is_running = "no"
            if jals.is_proc_running(cur_port):
                cur_port_is_running = "yes"
                num_running += 1

            print "------- port %i, running=%s----------" % (cur_port, cur_port_is_running)
            # print "------- stderr ----------"
            # print "".join(jals._read_pipe_file(cur_port, "stderr", require_exists=False))
            # print "------- stdout ----------"
            # print "".join(jals._read_pipe_file(cur_port, "stdout", require_exists=False))

        new_ports = 0
        while num_big_loops > 0 and jals.spawn():
            print "Spawning new port"
            new_ports += 1
        print "------- num new ports= %i ----------" % new_ports
        num_running += new_ports

        print "------- num running= %i ----------" % num_running
        num_big_loops -= 1

    jals.killall()
