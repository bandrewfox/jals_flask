#!python

# http://blog.corynissen.com/2013/05/using-r-to-communicate-via-socket.html

import socket
import time


def doit(txt):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(("localhost", 6012))

    print "sending text: %s" % txt
    client_socket.sendall(txt)
    data = client_socket.recv(256)
    print "Your upper cased text:  %s" % str(data)
    #client_socket.close()

doit("hello world and goodbye\n")
time.sleep(.3)
doit("real bye\n")

print "done"

# R code:
# tmout = 3600
# server <- function(){
#   while(TRUE){
#     writeLines(paste("Listening for",tmout,"seconds..."))
#     con <- socketConnection(host="localhost", port = 6012, blocking=TRUE, server=TRUE, open="r+", timeout = tmout)
#     data <- readLines(con,1)
#     print(data)
#     response <- toupper(data)
#     writeLines(response, con)
#     close(con)
#   }
# }
# server()


# while 1:
#     data = raw_input("Enter text to be upper-cased, q to quit\n")
#     client_socket.send(data)
#     if data == 'q' or data == 'Q':
#         client_socket.close()
#         break
#     else:
#         data = client_socket.recv(5000)
#         print "Your upper cased text:  %s" % str(data)
#

