from jals_obj import JustAnotherLiteServer


class Rjals(object):
    """
    This class stores a jals object, plus supplementary info.

    The primary purpose is to send code+data via the socket to the R servlet so that R can process it.  The servlet
    is meant to keep running in a stateful way to save loading times for quick requests.


    Further, it stores or looks up additional information about the R servlet running in each port, like:
      - which RData files or other objects are currently loaded in each one
      - which users have used each one
      - if the servlet is busy
      - the 'types' of requests and how long each request takes
    """

    def __init__(self, jals=None):
        assert isinstance(jals, JustAnotherLiteServer)
        self.jals = jals

        self.one_user_per_servlet = False

