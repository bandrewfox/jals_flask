import os
import subprocess
import socket

try:
    import psutil
except ImportError:
    psutil = None

# settings
COMMAND = "Rscript"
SCRIPT_PATH = "C:\\Users\\Brian\\Documents\\jals"
COMMAND_ARGS = [os.path.join(SCRIPT_PATH, "r_jals.R")]
MAX_PROCS = 4
STARTING_PORT = 6010


class JustAnotherLiteServer(object):

    def __init__(self, subdir, command=None, command_args=None):
        """
        make a new object
        :param subdir: temp dir to store all the stdout and stderr files, plus any other files needed by the scripts
        :param command: which command to run
        :param command_args: the args for the command. The port will be added as a final argument
        :return:
        """
        self._port2proc = {}
        self._port_file_numlines = {}

        # get defaults from settings
        if command is None:
            command = COMMAND
        if command_args is None:
            command_args = COMMAND_ARGS

        # make sure subdir exists and is writable
        if not subdir:
            raise ValueError("subdir required")
        if not os.path.isdir(subdir):
            raise ValueError("subdir not found: %s" % subdir)
        try:
            # make sure the dir is writable
            with open(os.path.join(subdir, "init.txt"), mode="w") as f:
                f.write("started\n")
        except Exception as e:
            raise OSError("Cannot write init.txt file in subdir: %s, error=%s" % (subdir, e))

        # set the args
        self.command = command
        self.command_args = command_args
        self.subdir = subdir

    @property
    def ports(self):
        return self._port2proc.keys()

    @property
    def procs(self):
        return self._port2proc.values()

    def _save_port(self, port, proc):
        """
        Save the newly made port/proc combo into this object,
        don't check if it is available (since it should have just been created/started)
        just do a last minute check to make sure it isn't already saved in the object
        :param port: newly bound port for spawned job
        :param proc: Popen object which is using the port
        :return: True if successful, it will raise an error if unsuccessful
        """
        if port in self.ports:
            raise ValueError("port already in use: %s" % str(port))
        if proc in self.procs:
            raise ValueError("proc already in use: %s" % str(proc))
        if len(self.procs) >= MAX_PROCS:
            raise ValueError("Max procs in use, cannot set new port: %s" % str(port))
        self._port2proc[port] = proc
        return True

    def find_available_port(self):
        """
        get an available port number (don't bind the port):
        (1) if fewer than max ports are allocated and (2) if the port number is available
        :return: port integer or None (if no ports are bindable)
        """
        if len(self.procs) < MAX_PROCS:
            check_port = STARTING_PORT
            while check_port in self.ports or self._is_port_already_bound(check_port):
                check_port += 1
            return check_port
        else:
            return None

    @staticmethod
    def _is_port_already_bound(port):
        """
        Try binding the port and if it fails then return True
        :param port: port integer
        :return: True/False
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.bind(("", port))
            s.close()
        except socket.error:
            return True

        return False

    @staticmethod
    def _concat_port_pipe_type(port, pipe_type):
        if pipe_type in ["stdout", "stderr"]:
            return "%s.%s" % (str(port), pipe_type)
        else:
            raise ValueError("pipe_type not allowed: %s" % str(pipe_type))

    def _get_pipe_filename(self, port, pipe_type):
        return os.path.join(self.subdir, "%s.txt" % self._concat_port_pipe_type(port, pipe_type))

    def _read_pipe_file(self, port, pipe_type, require_exists=True, return_lines=True):
        filename = self._get_pipe_filename(port, pipe_type)
        try:
            with open(filename, "r") as f:
                lines = f.readlines()
        except OSError as e:
            if require_exists:
                raise OSError("Cannot open file, error=%s" % e)
            else:
                lines = []

        self._port_file_numlines[self._concat_port_pipe_type(port, pipe_type)] = len(lines)
        if return_lines:
            return lines

    def does_pipe_file_have_more_lines(self, port, pipe_type):
        key_name = self._concat_port_pipe_type(port, pipe_type)
        numlines_old = self._port_file_numlines.get(key_name, 0)

        self._read_pipe_file(port, pipe_type, require_exists=False, return_lines=False)
        numlines_new = self._port_file_numlines.get(key_name, 0)

        if numlines_new > numlines_old:
            return True
        else:
            return False

    def _remove_port(self, port):
        # don't check if port is running, just update the class variables so that it is gone
        cur_proc = self._port2proc.get(port)
        if cur_proc is not None:
            del(self._port2proc[port])

    def is_proc_running(self, port):
        cur_proc = self._port2proc.get(port)
        if cur_proc is None:
            return False

        assert isinstance(cur_proc, subprocess.Popen)

        retval = cur_proc.poll()

        if retval is None:
            # proc is running
            return True

        # not running any more, so remove it from the proc list
        self._remove_port(port)
        if retval == 0:
            return False
        else:
            return False

    def spawn(self):
        """
        Find an available port and then start a new process using that port
        :return: port number of new process or None if there were no available ports
        """

        # get an available port
        port = self.find_available_port()
        if port is None:
            return None

        # open files and make file descriptors for stdout and stderr
        # using pipes would need this: http://stackoverflow.com/questions/375427/non-blocking-read-on-a-subprocess-pipe-in-python
        # cur_proc = subprocess.Popen(["Rscript.exe", "r_jals.R", str(port)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False, cwd=srcdir)
        stdout_filename = self._get_pipe_filename(port, "stdout")
        stderr_filename = self._get_pipe_filename(port, "stderr")
        try:
            fd_stdout = open(stdout_filename, "w")
        except Exception as e:
            raise Exception("Cannot open stdout file: %s" % stdout_filename)
        try:
            fd_stderr = open(stderr_filename, "w")
        except Exception as e:
            raise Exception("Cannot open stderr file: %s" % stderr_filename)

        # logging
        print "Spawning new script, port=%i, stout=%s, sterr=%s" % (port, stdout_filename, stderr_filename)

        cmd_and_args = [self.command]
        cmd_and_args.extend(self.command_args)
        cmd_and_args.append(str(port))
        try:
            cur_proc = subprocess.Popen(cmd_and_args, stdout=fd_stdout, stderr=fd_stderr, shell=False)
        except Exception as e:
            if str(e).find("The system cannot find the file specified") >= 0:
                raise RuntimeError("Cannot find command: %s, PATH=%s" % (cmd_and_args[0], os.environ['PATH']))
            else:
                raise Exception("Cannot open subprocess: %s" % e)

        print "done spawning, proc=%s" % str(cur_proc)

        if not self._save_port(port, cur_proc):
            cur_proc.kill()
            return None

        return port

    def killall(self):
        for cur_proc in self.procs:
            cur_proc.kill()

    def get_mem_usage_via_port(self, port):
        cur_proc = self._port2proc.get(port)
        if cur_proc is not None and psutil is not None:
            p = psutil.Process(cur_proc.pid)
            return p.memory_percent()
        else:
            return None

    @staticmethod
    def get_mem_usage_on_machine():
        if psutil is not None:
            return psutil.virtual_memory().percent
        else:
            return None


## do these in a different class
    def _get_idle_instance(self):
        # this is different from find_available_port, a new port is one which is not yet made (and fewer than the max number)
        # but a idle port is one which is responding to requests, i.e. not busy doing anything
        for cur_proc in self.procs:
            # check if the socket is busy
            pass

    def send_job_to_instance(self, job):
        idle_port = self._get_idle_instance()


